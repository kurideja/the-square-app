import React from 'react';
import './App.scss';
import { Square } from './Square/Square'
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom'
import { About } from './About/About';
import { Palette } from './Palette/Palette';

export const App = () => {
  return (
    <Router>
      <nav>
        <NavLink to="/">Black Square Online</NavLink>
        <NavLink to="/about">What is this?</NavLink>
        <NavLink to="/palette">Palette</NavLink>
      </nav>

      <div className="Container">
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/palette">
            <Palette />
          </Route>
          <Route path="/">
            <Square></Square>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}