import React from 'react';
import './LoadingIndicator.scss';

export const LoadingIndicator = () => {
  return <div className="LoadingIndicator">
    Loading
  </div>
};