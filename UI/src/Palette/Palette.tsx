import React from 'react';
import { ColorCard } from '../ColorCard/ColorCard';
import { palette } from './palette';
import './Palette.scss';

export const Palette = () => {
    return <div className="Container Palette">
        <p>
            First iteration of Black Square Online had a basic grayscale palette and it was getting boring.
            Current colors are sourced from photographs of Kazimir Malevich's paintings.
            If you are an artist and would like to improve what you see here, I am open for collaborations.
        </p>
        <p>
            Find me on <a href="https://twitter.com/kurideja" rel="noreferrer noopener" target="_blank">twitter</a>.
        </p>
        <div className="Palette-grid">
            {palette.map(color => <ColorCard key={color.toHexString()} color={color} />)}
        </div>
    </div>
}