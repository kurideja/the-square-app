import tinycolor2 from 'tinycolor2';

function generatePalette(): tinycolor2.Instance[] {
    const mainBlue = '#313058';
    const mainRed = '#982715';
    const mainLight = '#CDCECA';
    const mainDark = '#141414'

    const bluePalette = Array.from({ length: 4 }).map((v, i) => tinycolor2(mainBlue).darken(i * 3));
    const darkPalette = Array.from({ length: 5 }).map((v, i) => tinycolor2(mainDark).darken(i * 1));
    const redPalette = Array.from({ length: 4 }).map((v, i) => tinycolor2(mainRed).darken(i * 13));
    const lightPalette = Array.from({ length: 3 }).map((v, i) => tinycolor2(mainLight).darken(i * 35));

    return [...bluePalette, ...darkPalette, ...redPalette, ...lightPalette];
}

export const palette = generatePalette();