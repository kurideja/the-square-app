import React, { useEffect, useCallback, useState, useRef, memo, forwardRef } from 'react';
import './Square.scss';
import io from 'socket.io-client';
import FontFaceObserver from 'fontfaceobserver';
import { debounce, random, inRange } from 'lodash';
import { LoadingIndicator } from '../LoadingIndicator/LoadingIndicator';
import { palette } from '../Palette/palette';

const socket = io(window.location.href);

interface ICoords {
  x: number;
  y: number
}

interface INewLetter {
  key: string;
  x: number;
  y: number;
  fillStyle: string;
}

const Canvas = memo(forwardRef((props: { size: number }, ref: any) => {
  return <canvas className="Square" ref={ref as any} height={props.size as any} width={props.size as any}></canvas>
}));

export function Square() {
  const size = 1600;
  const squareRef = useRef<HTMLCanvasElement>(null);
  const locatorRef = useRef<HTMLDivElement>(null);
  const peerLocatorRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);
  const [color, setColor] = useState<string>('');
  const [coords, setCoords] = useState<ICoords>({ x: 0, y: 0 });
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const draw = useCallback(drawLetter, []);

  const showLocator = useCallback(({ x, y }: ICoords, locatorRef: React.RefObject<HTMLDivElement>) => {
    const locator = locatorRef.current;

    if (!locator || !squareRef.current) {
      return;
    }

    const scale = squareRef.current.clientWidth / size;

    locator.style.animation = 'none';

    window.requestAnimationFrame(() => {
      locator.style.display = 'block';
      locator.style.top = `${y * scale}px`;
      locator.style.left = `${x * scale}px`;
      locator.style.animation = '';
    });
  }, []);

  const randomize = useCallback(() => {
    setColor(getRandomColor());
    setCoords(getRandomCoords())
  }, []);

  const onKeyDownDebounced = useCallback(debounce(randomize, 1000), []);

  useEffect(() => {
    new FontFaceObserver('Playfair Display').load();

    setColor(getRandomColor);
    setCoords(getRandomCoords);

    const fetchSquare = async () => {
      const img = new Image();
      img.src = `/api/square?t=${Date.now()}`;
      img.onload = () => {
        const ctx = getCtx();

        if (ctx) {
          ctx.drawImage(img, 0, 0)
        }

        setIsLoading(false);
      };
    }

    fetchSquare();
  }, []);

  useEffect(() => {
    const input = inputRef.current;

    if (!input) {
      return;
    }

    const onFocus = () => {
      input.style.position = 'fixed'
      setTimeout(() => squareRef.current && squareRef.current.scrollIntoView(), 300);
    };

    const onBlur = () => {
      input.style.position = ''
    };

    const onInput = () => {
      input.value = ''
    }

    input.addEventListener('focus', onFocus);
    input.addEventListener('blur', onBlur);
    input.addEventListener('input', onInput);

    return () => {
      input.removeEventListener('focus', onFocus);
      input.removeEventListener('blur', onBlur);
      input.removeEventListener('input', onInput);
    };
  });

  useEffect(() => {
    socket.connect();

    socket.on('from server', (newLetter: INewLetter) => {
      draw(newLetter);
      showLocator({ x: newLetter.x, y: newLetter.y }, peerLocatorRef);
    });

    return () => { socket.disconnect(); }
  }, [draw, showLocator])

  useEffect(() => {
    const onKey = ({ key = '' }) => {
      if (key.length > 1) {
        // Various command keys etc should be removed from inputs.
        return;
      }

      onKeyDownDebounced();

      const { x, y } = nextCoords(coords);
      const letter = { key, x, y, fillStyle: color };

      draw(letter);
      showLocator({ x, y }, locatorRef);
      setCoords({ x, y });

      socket.emit('new message', letter);
    }

    document.addEventListener('keydown', onKey);

    return () => {
      document.removeEventListener('keydown', onKey);
    }
  }, [draw, color, onKeyDownDebounced, showLocator, coords]);

  function getRandomColor() {
    return palette[random(palette.length - 1)].toHexString();
  }

  function getRandomCoords(): ICoords {
    return { x: random(size), y: random(size) };
  }

  function nextCoords({ x, y }: ICoords) {
    const deltaX = random(13, 55);
    const deltaY = random(-34, 34);

    const newX = inRange(x + deltaX, 0, size) ? x + deltaX : x + deltaX - size;
    const newY = inRange(y + deltaY, 0, size) ? y + deltaY : Math.abs(deltaY);

    return {
      x: newX,
      y: newY
    }
  }

  function getCtx(): CanvasRenderingContext2D | null {
    return squareRef.current && squareRef.current.getContext('2d')!;
  }

  function drawLetter({ key, x, y, fillStyle }: INewLetter): void {
    const ctx = getCtx();

    if (!ctx) {
      return;
    }

    ctx.font = 'italic 26px Playfair Display';
    ctx.fillStyle = fillStyle;
    ctx.fillText(key, x, y);
  }

  return (
    <div>
      <div id="locator" ref={locatorRef} className="Locator"></div>
      <div id="peerLocator" ref={peerLocatorRef} className="Locator Locator--peer"></div>
      {isLoading && <LoadingIndicator />}
      <Canvas ref={squareRef} size={size} />
      <div className="InputContainer">
        <p>No keyboard? Click on the input below.</p>

        <input className="Input" ref={inputRef} type="text" autoComplete={'off'} name="black-square" />
      </div>
    </div>
  )
}
