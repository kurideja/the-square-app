import React from 'react';
import './About.scss';

export const About = () => {
  return <div className="Container About">
    <h1>About the Black Square Online</h1>
    <p>
      Hi there! I am <a href="https://twitter.com/kurideja" rel="noreferrer noopener" target="_blank">Gytis</a>, developer of this project. Please follow this link to Medium to learn more: <a href="https://kurideja.medium.com/about-the-black-square-online-3fe2cdb39a53" rel="noreferrer noopener" target="_blank">About the Black Square Online</a>.
    </p>
    <h2>Disclaimer</h2>
    <ul>
      <li>
        The black square you saw on the main page is created by anonymous visitors.
        </li>
      <li>
        All the input (individual letters, their placement, color and timestamp) is stored in a secured database that is not publicly available.
        </li>
      <li>
        There's no 3rd party tracking installed (no Google, Facebook etc.).
        </li>
      <li>
        All updates coming from you or others are real-time and journal-based, meaning no <i>backspace</i> key for you.
        </li>
      <li>
        Collaborations with artists, if any, will be announced here. Nothing, even your IP or approximate location, is stored or could be used by others.
        </li>
      <li>
        Rule of thumb: share as much as you would share in a message in a bottle.
        </li>
    </ul>
  </div>
}