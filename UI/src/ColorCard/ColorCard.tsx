import React from 'react';
import './ColorCard.scss';
import tinycolor2 from 'tinycolor2';

export const ColorCard = (props: { color: tinycolor2.Instance }) => {
    const { color } = props;

    return <div className="ColorCard">
        <div className="ColorCard-preview" style={{ backgroundColor: color.toHexString() }}></div>
        <div className="ColorCard-name">{color.toHexString()}</div>
    </div>
}