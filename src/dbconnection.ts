import { connect } from 'mongoose';

require('mongoose').Promise = require('bluebird');

const {
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  MONGO_INITDB_DATABASE,
  MONGO_PORT,
  MONGO_HOST
} = process.env;

const url = `mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_INITDB_DATABASE}`;
const connectionConfig = {
  useNewUrlParser: true, useUnifiedTopology: true, user: DATABASE_USERNAME, pass: DATABASE_PASSWORD,
};

export const db = connect(url, connectionConfig);