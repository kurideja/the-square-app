import { Letter } from "../models/Letter.schema";
import { registerFont, createCanvas, loadImage, Canvas } from "canvas";
import { ILetter } from "../models/Letter.interface";

registerFont('src/assets/PlayfairDisplay-Italic.otf', { family: 'Playfair Display' });

const size = 1600;
const canvas = createCanvas(size, size);
const ctx = canvas.getContext('2d');

export const getSnapshot = () => canvas;

export const appendToSnapshot = (letter: ILetter) => {
  drawLetter(letter);
}

export function createInitialSnapshot(): Promise<void> {
  ctx.fillStyle = '#ffffff';
  ctx.fillRect(0, 0, size, size);

  const lettersCursor = Letter.find().lean().cursor();

  return new Promise((resolve) => {
    lettersCursor.on('data', drawLetter).on('close', () => {
      resolve()
    });
  })
}

function drawLetter(letter: ILetter) {
  ctx.font = 'italic 26px Playfair Display';
  ctx.fillStyle = letter.fillStyle;
  ctx.fillText(letter.key, letter.x, letter.y)
}