import { Router } from 'express';
import { square } from './square';

export const routes = Router();

routes.use('/square', square);

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Connected!' });
});
