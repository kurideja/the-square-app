import { Router } from 'express';
import { current } from './current';

export const square = Router();

square.get('/', current);