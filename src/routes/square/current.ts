import { getSnapshot } from "../../services/createSnapshot";

export const current = async (req, res) => {
  res.setHeader('Content-Type', 'image/jpeg');
  getSnapshot().createJPEGStream().pipe(res);
}