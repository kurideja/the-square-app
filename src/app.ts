import express from 'express';
import { createServer } from 'http';
import SocketIO from 'socket.io';
import { Letter } from './models/Letter.schema';
import { db } from './dbconnection';
import { routes } from './routes';
import isHexColor from 'is-hexcolor'
import { createInitialSnapshot, appendToSnapshot } from './services/createSnapshot';

const app = express();
const port = 3011;
const server = createServer(app);
const io = SocketIO(server);

const init = async () => {
  await db;
  await createInitialSnapshot();

  io.on('connection', (socket) => {
    socket.join('letters');

    socket.on('new message', (props) => {
      if (!isValidMessage(props)) {
        return;
      }

      socket.broadcast.to('letters').emit('from server', props);

      db.then(db => {
        const letter = new Letter(props);
        letter.save();
        appendToSnapshot(props);
      }).then(() => { }, err => console.log(err))
    });

    socket.on('disconnect', () => {
      socket.leave('letters');
    })
  });

  server.listen(port);

  app.use('/api', routes);
}

(async () => await init())();

function isValidMessage(props) {
  if (!props) {
    return false;
  }

  const key = props.key;
  const isValidKey = typeof key === 'string' && key.length === 1;
  const isValidPosition = Number.isInteger(props.x) && Number.isInteger(props.y);
  const isValidFillStyle = isHexColor(props.fillStyle);

  return isValidKey && isValidPosition && isValidFillStyle;
};