import { model, Schema } from 'mongoose';
import { ILetter } from './Letter.interface';

const letterSchema = new Schema(
  {
    key: {
      type: String
    },
    x: {
      type: Number
    },
    y: {
      type: Number
    },
    fillStyle: {
      type: String
    }
  },
  { timestamps: true }
)

export const Letter = model<ILetter>('Letter', letterSchema);
