import { Document } from "mongoose";

export interface ILetter extends Document {
  key: string;
  x: number;
  y: number;
  fillStyle: string;
  createdAt: number;
}
