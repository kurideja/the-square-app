FROM node:10

WORKDIR /usr/src/app

COPY . .

RUN yarn install

RUN yarn tsc

EXPOSE 3011

ENTRYPOINT ["node", "dist/app.js"]